import { Component } from '@angular/core';
import { PostService } from '../post.service';
import { Observable } from 'rxjs/internal/Observable';
import { SharedPostService } from '../shared-post.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})




export class CreatePostComponent {

  uploadImage!: File;
  imageUrl: any;
  imagefile: any
  postResponse: any;
  dbImage: any;
  constructor(private postService: PostService, private httpClient: HttpClient,private router:Router) {
  }

  uploadImageToFile(event: any) {
    this.uploadImage = event.target.files[0];
  }

  

  save() {
    let formData = new FormData();
    formData.append('image', this.uploadImage, this.uploadImage.name);
    this.postService.uploadImage(formData).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.getImage();
        this.router.navigate(['/post'])
      }
    })
  }


  getImage() {
    const name = this.uploadImage.name;
    this.postService.getUploadImage(name).subscribe((data:any)=>{
      this.postResponse = data;
        this.dbImage = 'data:image/jpeg;base64,' + this.postResponse.filePath;
    })
  }
}
