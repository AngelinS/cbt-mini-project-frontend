// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
// // import { User } from '../user.model'; // Verify the import path
// import { SharedPostService } from '../shared-post.service';
// import { User } from '../user';

// @Component({
//   selector: 'app-user-list',
//   templateUrl: './user-list.component.html',
//   styleUrls: ['./user-list.component.css']
// })
// export class UserListComponent implements OnInit {
//   // user: any;

//   // constructor(private userService: UserService) {}

//   // ngOnInit() {
//   //   // Call the service to get the user details
//   //   this.userService.getUserDetails().subscribe(
//   //     (data) => {
//   //       this.user = data;
//   //     },
//   //     (error) => {
//   //       console.error('Error fetching user details:', error);
//   //     }
//   //   );
//   // }

//   user: User = new User(); // Initialize as an empty User object

//   User!: User;
//   isFollowing: boolean = false;
//   currentUser!: User; // Add this property to store the current user

//   constructor(private dataSharingService: SharedPostService) {}

//   ngOnInit() {
//     this.dataSharingService.userData$.subscribe((user) => {
//       if (user) {
//         this.user = user;
//         this.checkIfFollowing();
//       }
//     });
//   }

//   followUser() {
//     // Implement follow functionality here
//     this.isFollowing = true; // Set the flag to indicate the user is now following
//   }

//   // Add a method to check if the current user is following this user
//   checkIfFollowing() {
//     // Replace this with your actual logic to check if the current user is following the displayed user
//     // For example, you can compare the IDs of the current user and the displayed user
//     if (this.currentUser && this.user && this.currentUser.id === this.user.id) {
//       this.isFollowing = true;
//     } else {
//       this.isFollowing = false;
//     }
//   }
// }
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
// import { User } from '../user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
 export class UserListComponent  {
  // users: User[] = [];

  // constructor(private userService: UserService) {}

  // ngOnInit() {
  //   this.userService.getUsers().subscribe(
  //     (users) => {
  //       this.users = users;
  //     },
  //     (error) => {
  //       console.error('Error fetching users:', error);
  //       // Handle error, e.g., display an error message to the user
  //     }
  //   );
  // }
}
