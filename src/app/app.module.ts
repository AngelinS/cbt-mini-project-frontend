import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { FollowingComponent } from './following/following.component';
import { AllpostComponent } from './allpost/allpost.component';
import { UserComponent } from './user/user.component';
import {MatIconModule} from '@angular/material/icon';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
// import {MatTableModule} from '@angular/material/table';

// import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FollowersComponent } from './followers/followers.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { PostComponent } from './post/post.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { RouterModule } from '@angular/router';
import { PostService } from './post.service';
import { SharedPostService } from './shared-post.service';
// import { UserListComponent } from './user-list/user-list.component';
import { FollowComponent } from './follow/follow.component';
import { UserDataService } from './user-data.service';
// import { UserDetailsComponent } from './userdetails/userdetails.component';
import { UserListComponent } from './user-list/user-list.component';
// import { UserService } from './user.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    FollowersComponent,
    FollowingComponent,
    AllpostComponent,
    UserComponent,
    CreatePostComponent,
    PostComponent,
    ImageUploadComponent,
    UserListComponent,
    FollowComponent,
    // UserDetailsComponent,
    
    
    UserComponent,
    
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule
  ],
  providers: [PostService,SharedPostService,UserDataService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
