import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-followers',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent  {
ang:any
username!:String;
  email!: String;
constructor(
  private userService: UserService
){}

 ELEMENT_DATA : PeriodicElement[]=[];

ngOnInit(){
  this.getFollowing()
}
  getFollowing(){
     
      this.userService.getFollowing(localStorage.getItem('username')!).subscribe({
        next:(res:any)=>{
          this.ang=res;
          res.map((a:any)=>this.ELEMENT_DATA.push(a));
          this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        }
        
      });
      
  }
  displayedColumns: string[] = ['username','email'];
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA)

  
}
export interface PeriodicElement {
  username:String;
  email: String;
}
