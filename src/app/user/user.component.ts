import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
// import { User } from '../user';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
// import { User } from '../user';
import { SharedPostService } from '../shared-post.service';
// import { User } from '../user.model';
import { UserDataService } from '../user-data.service';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  
  ang:any
username!:String;
  email!: String;
  fll: any;
constructor(
  private userService: UserService
){}

 ELEMENT_DATA : PeriodicElement[]=[];

ngOnInit(){
  this.getUser()
}
getUser(){
     
      this.userService.getUser().subscribe({
        next:(res:any)=>{
          this.ang=res;
          res.map((a:any)=>this.ELEMENT_DATA.push(a));
          this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        }
        
      });
      
  }
  displayedColumns: string[] = ['username'];
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA)

  
  
  follow(userId:any,){
     
        this.userService.followUser(localStorage.getItem('followerid'),userId).subscribe({
          next:(foll:any)=>{
            this.fll=foll;
            
          }
        })
  }

  unfollow(userId:any){
     
    this.userService.unfollowUser(localStorage.getItem('followerid'),userId).subscribe({
      next:(res:any)=>{
       
      }
    })
}
}
export interface PeriodicElement {
  username:String;
}

