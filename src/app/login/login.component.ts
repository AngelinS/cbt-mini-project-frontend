import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string ="";
  password: string ="";
  user:User=new User();
asd:any;
  constructor(private router: Router,private http: HttpClient,private userService:UserService) {}
 
  // Login():void {
  //   this.router.navigate(['/profile']);
  //   console.log(this.email);
  //   console.log(this.password);
 
  //   let bodyData = {
  //     email: this.email,
  //     password: this.password,
  //   };  

    // const headers = new HttpHeaders(bodyData ? { authorization: 'Basic ' + btoa(bodyData.email + ':' + bodyData.password) } : {});
    //     this.http.post("http://localhost:8080/login", headers).subscribe(  (resultData: any) => {
    //     console.log(resultData);
      
    //   });
    // }

  Login(){
    this.userService.login(this.user).subscribe((data)=>{
      console.log(data);
      this.asd=data;
     localStorage.setItem("username",this.asd.username);
     localStorage.setItem("followerid",this.asd.userid);
      this.router.navigate(['/profile']);
    },(error)=>{
      alert("Error")
    })
  }
}
