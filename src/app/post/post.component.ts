import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { SharedPostService } from '../shared-post.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
 
  constructor(private postservice:PostService){}
  post:any;

  imageUrl: any;
  imagefile: any;
  postResponse: any;
  dbImage: any;
  imgFile = [];

  ngOnInit(): void {
    this.getImages()
  }
  getImages() {
    this.postservice.getall().subscribe(
      (val) => {
       this.post=val;
      
        console.log(this.post)
        this.post.forEach((element: { filePath: any; }) => {
          element.filePath 
          element.filePath = 'data:image/jpeg;base64,'+element.filePath;
        });
        
      }
    );
  }

}