import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  selector: 'app-allpost',
  templateUrl: './allpost.component.html',
  styleUrls: ['./allpost.component.css']
})
export class AllpostComponent implements OnInit {
  constructor(private postservice:PostService){}
  post:any;

  imageUrl: any;
  imagefile: any;
  postResponse: any;
  dbImage: any;
  imgFile = [];

  ngOnInit(): void {
    this.getImages()
  }
  getImages() {
    this.postservice.getall().subscribe(
      (val) => {
       this.post=val;
      
        console.log(this.post)
        this.post.forEach((element: { filePath: any; }) => {
          element.filePath 
          element.filePath = 'data:image/jpeg;base64,'+element.filePath;
        });
        
      }
    );
  }


}
