import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class PostService {
  
  




  private apiUrl = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  uploadImage(reqParam: FormData) {
    return this.httpClient.post(this.apiUrl + '/image/fileSystem', reqParam, { observe: 'response' });

  }

  getImage(id: number): Observable<Blob> {
    return this.httpClient.get(`${this.apiUrl}/image/fileSystem/${id}`, { responseType: 'blob' });
  }

  getUploadImage(name: string) {
   return this.httpClient.get(this.apiUrl+'/image/get/image/info/'+name);
  }
  
  getall(){
    return this.httpClient.get(this.apiUrl+'/image/getAll');
  }
 
}



