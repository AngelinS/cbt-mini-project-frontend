// import { Injectable } from '@angular/core';
// import { User } from './user';

// @Injectable({
//   providedIn: 'root'
// })
// export class UserService {
//   createUser(username: string, email: string) {
//     throw new Error('Method not implemented.');
//   }
//   private users: User[] = [
//     new User(1, 'user1', 'User One', 'https://bootdey.com/img/Content/avatar/avatar6.png', 'user1@example.com'),
//     new User(2, 'user2', 'User Two', 'https://bootdey.com/img/Content/avatar/avatar1.png', 'user2@example.com'),
//     new User(3, 'user3', 'User3 ', 'https://bootdey.com/img/Content/avatar/avatar3.png', 'user3@example.com'),
//     new User(4, 'user4', 'User4 ', 'https://bootdey.com/img/Content/avatar/avatar2.png', 'user4@example.com'),
//     new User(5, 'user5', 'User5 ', 'https://bootdey.com/img/Content/avatar/avatar5.png', 'use5r@example.com'),
//     new User(6, 'user6', 'User 6', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'user6@example.com'),
//     new User(7, 'user7', 'User 7', 'https://bootdey.com/img/Content/avatar/avatar8.png', 'use7r@example.com'),
//     new User(8, 'user8', 'User8 ', 'https://bootdey.com/img/Content/avatar/avatar3.png', 'user8@example.com'),
//     new User(9, 'user9', 'User9 ', 'https://bootdey.com/img/Content/avatar/avatar4.png', 'user9@example.com'),
//     new User(10, 'user10', 'User10 ', 'https://bootdey.com/img/Content/avatar/avatar2.png', 'user10@example.com')

//   ];
  

//   getUsers(): User[] {
//     return this.users;
//   }

//   getUserById(id: number): User | undefined {
//     return this.users.find(user => user.id === id);
//   }
// }




import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';
// import { User } from './user.model';
// import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // [x: string]: any;
  // private baseUrl = 'http://url';

  constructor(private http: HttpClient) { }

  // createUser(user: any) {
  //   // Send an HTTP POST request to create a user
  //   const url = `${this.baseUrl}/api/users/create`;
  //   return this.http.post(url, user);
  // }

  // getUsers() {
  //   // Send an HTTP GET request to fetch the list of users
  //   const url = `${this.baseUrl}/api/users`; 
  //   return this.http.get<any[]>(url);
  // }




  // private baseUrl = 'http://url'; 

  // constructor(private http: HttpClient) {}

  // createUser(user: any): Observable<any> {
  //   return this.http.post(`${this.baseUrl}/create`, user);
  // }

  // getUserDetails(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/user-list`);
  // }



  // private baseUrl = '/api/users'; 

  // constructor(private http: HttpClient) {}

  // createUser(user: any): Observable<any> {
  //   return this.http.post<any>(`${this.baseUrl}/create`, user);
  // }




  // private apiUrl = '/api/users'; 

  // constructor(private http: HttpClient) {}

  // createUser(user: User): Observable<User> {
  //   return this.http.post<User>(`${this.apiUrl}/create`, user);
  // }

  // getUsers(): Observable<User[]> {
  //   return this.http.get<User[]>(`${this.apiUrl}`);
  // }


  // private apiUrl = '/api/users';

  // constructor(private http: HttpClient) {}

  // createUser(user: User): Observable<User> {
  //   return this.http.post<User>(`${this.apiUrl}/create`, user);
  // }

  // getUsers(): Observable<User[]> {
  //   return this.http.get<User[]>(`${this.apiUrl}`);

    private url='http://localhost:8080/login'
    login(user:User){
      return this.http.post(`${this.url}`,user)
    }


    private followerurl='http://localhost:8080/api/users/followers/'
    getFollower(follower:String){
      return this.http.get(`${this.followerurl}`+follower)
    }
    private followingurl='http://localhost:8080/api/users/following/'
    getFollowing(following:String){
      return this.http.get(`${this.followingurl}`+following)
    }


    private userurl='http://localhost:8080/getAllUser'
    getUser(){
      return this.http.get(`${this.userurl}`)
    }

    private followurl='http://localhost:8080/api/users/follow'
    followUser(followerId:any, followingId: any){
      const params={
        followerId:followerId,
        followingId:followingId,
      }
      return this.http.get(`${this.followurl}`,{params:params})
    }
    private unfollowurl='http://localhost:8080/api/users/unfollow'
    unfollowUser(followerId:any, followingId: any){
      const params={
        followerId:followerId,
        followingId:followingId,
      }
      return this.http.delete(`${this.unfollowurl}`,{params:params})
    }
  }

